# instala la versión más pequeña de la imagen
FROM node:stretch-slim
# crea el directorio de la aplicación
RUN mkdir -p /usr/src/reto_devops
#selecciona la carpeta creada
WORKDIR /usr/src/reto_devops
#copia el index en la carpeta de /usr/src/reto_devops
COPY index.js /usr/src/reto_devops
#copia el archivo json dentro de la carpeta creada
COPY package*.json ./ 
#instala las depencias
RUN npm install
#copia todo el contenido de la carpeta en la imagen
COPY . .
#expone el puerto
EXPOSE 3000
#ejecuta la aplicación
CMD node index.js