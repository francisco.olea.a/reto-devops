# Reto-DevOps CLM
![CLM Consoltores](./img/clm.png)

## La app
![NodeJs](./img/nodejs.png)

```bash
$ git clone https://gitlab.com/francisco.olea.a/reto-devops.git

$ cd ./reto-devops

```
### Instalar Dependencias
```bash
$ npm install
```
### Ejecutar Test
![Jest](./img/jest.jpg)

```bash
$ npm run test
```

### Ejecutar la app
```bash
$ node index.js
Example app listening on port 3000!
```
### Reto 1. Dockerize la aplicación
![docker](./img/nodedoker.jpg)

```bash
$ docker build -t reto-devops .
$ docker run -p 3000:3000 reto-devops
```

### Reto 2. Docker Compose
![compose](./img/docker-compose.png)

```bash
$ docker-compose build
$ docker-compose up
```

